﻿namespace StateOfTheTeam.Presentation.Controllers
{
    using System.Web.Http;

    using StateOfTheTeam.Presentation.Infrastructure;
    using StateOfTheTeam.Presentation.Models;

    public class BuildStateController : ApiController
    {
        public BuildState Get(string buildTypeId)
        {
            var teamCity = TeamCity.Current;
            return teamCity.GetState(buildTypeId);
        }
    }
}