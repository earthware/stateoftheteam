﻿namespace StateOfTheTeam.Presentation.Controllers
{
    using System.Collections.Generic;
    using System.Web.Http;

    using StateOfTheTeam.Presentation.Infrastructure;
    using StateOfTheTeam.Presentation.Models;

    public class BuildTypeController : ApiController
    {
        public IEnumerable<BuildType> Get()
        {
            var teamCity = TeamCity.Current;
            return teamCity.GetAllBuildTypes();
        }
    }
}