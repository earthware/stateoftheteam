﻿namespace StateOfTheTeam.Presentation.Infrastructure
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Xml.Linq;

    using StateOfTheTeam.Presentation.Models;

    public class TeamCity
    {
        private const string BaseUrl = "{0}://{1}:{2}/httpAuth/app/rest/{3}";

        private static readonly object SyncRoot = new object();

        private static volatile TeamCity instance;

        private readonly string password;

        private readonly string port;

        private readonly string server;

        private readonly string userName;

        private TeamCity(string server, string port, string userName, string password)
        {
            this.server = server;
            this.port = port;
            this.userName = userName;
            this.password = password;
        }

        public static TeamCity Current
        {
            get
            {
                if (instance == null)
                {
                    lock (SyncRoot)
                    {
                        if (instance == null)
                        {
                            instance = new TeamCity("192.168.11.121", "8888", "test", "password");
                        }
                    }
                }

                return instance;
            }
        }

        public IEnumerable<BuildType> GetAllBuildTypes()
        {
            var url = string.Format(BaseUrl, "http", this.server, this.port, "buildTypes/");
            var data = this.GetData(url);

            var result = from xml in data.Elements("buildTypes").Elements("buildType")
                         select
                             new BuildType
                                 {
                                     Id = xml.Attribute("id").Value, 
                                     Name = xml.Attribute("name").Value, 
                                     ProjectName = xml.Attribute("projectName").Value, 
                                     ProjectId = xml.Attribute("projectId").Value
                                 };

            return result.ToList();
        }

        public BuildState GetState(string buildTypeId)
        {
            var urlSuffix = string.Format("buildTypes/id:{0}/builds/?count=1&running:any", buildTypeId);
            var url = string.Format(BaseUrl, "http", this.server, this.port, urlSuffix);
            var data = this.GetData(url);

            var build = data.Elements("builds").Elements("build").FirstOrDefault();

            var status = BuildStatus.Unknown;

            if (build != null)
            {
                switch (build.Attribute("status").Value)
                {
                    case "SUCCESS":
                        status = BuildStatus.Good;
                        break;
                    case "FAILURE":
                        status = BuildStatus.Broken;
                        break;
                    default:
                        status = BuildStatus.Unknown;
                        break;
                }
            }

            return new BuildState { BuildTypeId = buildTypeId, Status = status };
        }

        private XDocument GetData(string url)
        {
            var request = (HttpWebRequest)WebRequest.Create(url);
            request.Credentials = new NetworkCredential(this.userName, this.password);
            request.Method = WebRequestMethods.Http.Get;

            using (var response = request.GetResponse())
            {
                using (var stream = response.GetResponseStream())
                {
                    var doc = XDocument.Load(stream);
                    return doc;
                }
            }
        }
    }
}