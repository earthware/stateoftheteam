steal(
    "jquery/controller",
    "jquery/controller/view",
    "jquery/view/ejs",
    "app/models",
    "app/lib/jquery.ba-dotimeout.min.js"
)
.then(
    "./views/init.ejs",
    "./views/build_type.ejs",
    function () {
        $.Controller("App.Project.List", {
            defaults: {}
        }, {
            init: function () {
                    var project, a;
                App.Models.BuildType.findAll().then(this.callback(function (data) {
                    App.UserSettings.get().then(this.callback(function (settings) {
                        this.projects = $.grep(data, function (elt, i) {
                            return !settings.isHidden(elt.id);
                        });
                        
                        this.element.html("//app/project/list/views/init.ejs", this.projects, this.callback(function () {
                            this.bindEventHandlers();
                            this.startUpdates();
                        }));
                    }));
                }));
            },
            
            grouping: function() {
                var other = {};
                $.each(this.projects, function (i, val) {
                    this.project = val.projectName;
                    console.log(val);
                    if (!(this.project in other)) {
                        other[this.project] = [];
                    }

                    other[this.project].push(val);
                });

                return other;
            },

            bindEventHandlers: function () {
                for (var i = 0; i < this.projects.length; i++) {
                    this.projects[i].bind("state", this.callback(function (ev, val) {
                        var model = ev.target;
                        var elt = model.elements(this.element);
                        var newElt = this.view("build_type", model);
                        elt.replaceWith(newElt);
                    }));
                }
            },

            startUpdates: function () {
                App.UserSettings.get().then(this.callback(function(settings) {
                    var timeout = settings.refreshInterval * 1000;

                    $.doTimeout("refreshBuilds", timeout, this.callback(this.updateProjects));
                    $.doTimeout("refreshBuilds", true);
                }));
            },

            updateProjects: function () {
                for (var i = 0; i < this.projects.length; i++) {
                    this.projects[i].update();
                }
                
                return true;
            },
            
            "li.btn click": function (ev) {
                var currentStateString = "li." + ev.attr("id");
                var currentState = $(currentStateString);
                ev.attr('unselectable', 'on');
                ev.css('user-select', 'none');
                ev.on('selectstart', false);
                
                switch (ev.attr('class')) {
                    case 'btn on':
                        ev.addClass("off");
                        ev.removeClass("on");
                        currentState.toggle();
                        break;
                    case 'btn off':
                        ev.addClass("on");
                        ev.removeClass("off");
                        currentState.toggle();
                        break;
                }
            },

            "div.fullPage click": function (ev) {
                var docElement, request;

                docElement = document.documentElement;
                request = docElement.requestFullScreen || docElement.webkitRequestFullScreen || docElement.mozRequestFullScreen || docElement.msRequestFullScreen;

                if (typeof request != "undefined" && request) {
                    request.call(docElement);
                }
                
            }
        });

    });