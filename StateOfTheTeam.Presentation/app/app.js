steal(
    "steal/less",
    "./project/list/list.js")
.then(
    "./styles/reset.css",
    "./styles/app.less",
    function () {
        $("#content").app_project_list();
    });