steal(
    "jquery/model")
.then(
    function () {
        $.Model("App.Models.BuildType", {
            findAll: "/api/buildtype"
        }, {
            init: function () {
                this.state = 0;
            },

            update: function () {
                var buildStateLink = $.grep(this.links, function (elt, i) {
                    return elt.rel === "BuildState";
                });

                if (buildStateLink.length === 0) {
                    return $.Deferred().reject();
                }

                var stateRequest = $.get(buildStateLink[0].href);
                
                stateRequest.then(this.callback(function (data) {
                    this.attr("state", data.status);
                }));
            }
        });
    });