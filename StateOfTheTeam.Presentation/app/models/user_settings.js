steal(
    "jquery/model")
.then(
    function () {
        $.Model("App.UserSettings", {
            findOne: function () {
                var model = undefined;
                var result;

                if (localStorage) {
                    var data = localStorage.settings;
                    if (data) {
                        model = JSON.parse(data);
                    }
                }

                if (model) {
                    result = new App.UserSettings(model);
                } else {
                    result = new App.UserSettings();
                }

                return $.Deferred().resolve(result);
            },

            get: function () {
                return this.findOne();
            },

            findAll: function () {
                var result = $.Deferred();

                this.findOne(), then(function (data) {
                    result.resolve([data]);
                });

                return result;
            },

            create: function (attrs, success, error) {
                attrs["id"] = "settings";

                if (localStorage) {
                    localStorage.settings = JSON.stringify(attrs);
                } else {
                    error();
                }

                success({ id: "settings" });
            },

            update: function (id, attrs, success, error) {
                if (localStorage) {
                    localStorage.settings = JSON.stringify(attrs);
                    success();
                } else {
                    error();
                }
            }
        }, {
            init: function (attrs) {
                this.attrs($.extend({
                    refreshInterval: 30,
                    hiddenBuildTypes: []
                }, attrs));
            },

            isHidden: function (buildType) {
                var index = $.inArray(buildType, this.hiddenBuildTypes);

                return index !== -1;
            }
        });
    });