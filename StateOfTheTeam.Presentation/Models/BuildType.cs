namespace StateOfTheTeam.Presentation.Models
{
    using System.Diagnostics;

    using Newtonsoft.Json;

    [DebuggerDisplay("{ProjectName} - {Name}")]
    public class BuildType
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("projectName")]
        public string ProjectName { get; set; }

        [JsonProperty("projectId")]
        public string ProjectId { get; set; }

        [JsonProperty("links")]
        public Link[] Links
        {
            get
            {
                return new[] { Link.BuildState(this.Id) };
            }
        }
    }
}