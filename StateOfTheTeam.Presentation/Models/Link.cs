﻿namespace StateOfTheTeam.Presentation.Models
{
    using Newtonsoft.Json;

    public class Link
    {
        [JsonProperty("href")]
        public string Href { get; set; }

        [JsonProperty("rel")]
        public string Rel { get; set; }

        public static Link BuildState(string buildTypeId)
        {
            return new Link { Rel = "BuildState", Href = string.Format("/api/buildtype/{0}/state", buildTypeId) };
        }
    }
}