﻿namespace StateOfTheTeam.Presentation.Models
{
    public enum BuildStatus
    {
        Unknown = 0, 

        Good = 1,

        Broken = 2,

        Building = 3,

        Hanging = 4
    }
}