﻿namespace StateOfTheTeam.Presentation.Models
{
    using Newtonsoft.Json;

    public class BuildState
    {
        [JsonProperty("buildTypeId")]
        public string BuildTypeId { get; set; }

        [JsonProperty("status")]
        public BuildStatus Status { get; set; }
    }
}